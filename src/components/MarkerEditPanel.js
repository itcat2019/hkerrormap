import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { makeStyles, useTheme } from '@material-ui/styles';
import { Card, CardHeader, CardContent, Grid, Button, TextField, Typography } from '@material-ui/core';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import IconButton from '@material-ui/core/IconButton';
import SaveIcon from '@material-ui/icons/Save';
import CancelIcon from '@material-ui/icons/Cancel';
import DeleteIcon from '@material-ui/icons/Delete';
import { Picker, Emoji, emojiIndex } from 'emoji-mart';
import { withFormik } from "formik";
import { every, isEmpty } from 'lodash';
import TimeAgo from 'timeago-react';

import 'emoji-mart/css/emoji-mart.css';

const useStyles = makeStyles(theme => ({
    controlRight: {
        backgroundColor: 'rgba(255,255,255,0.8)',
        position: 'fixed',
        height: 'calc(100vh - 64px)',
        width: '40%',
        maxWidth: 400,
        right: 0,
        top: 64,
    },
    controlBottom: {
        backgroundColor: 'rgba(255,255,255,0.8)',
        position: 'fixed',
        height: '40%',
        width: '100%',
        bottom: 0,
        display: 'flex',
        flexDirection: 'column'
    },
    cardContent: {
        height: "100%",
        overflow: "scroll"
    },
    iconCard: {
        cursor: 'pointer',
        marginTop: 20,
        width: 40,
        height: 40,
        display: 'flex',
        alignContent: 'center',
        justifyContent: 'center',
    },
    marginTopItem: {
        marginTop: 30,
    },
    rightIcon: {
        marginLeft: theme.spacing(1),
    },
    button: {
        margin: theme.spacing(1),
    },
    iconButton: {
        width: '3rem',
        fontSize: '2rem',
    },
    formControl: {
        minWidth: 120,
    },
}));


const getEmoji = (id) => {
    const foundEmoji = emojiIndex.search(id).filter(m => m.id === id);
    if (foundEmoji.length > 0) {
        return foundEmoji[0];
    }
    return null;
}

const InnerForm = ({ layers, values, errors, handleChange, handleSubmit, handleCancel, handleDelete, isSubmitting, setFieldValue, dirty }) => {

    const classes = useStyles();

    // const [shownIconPicker, setShownIconPicker] = useState(false);
    // const [emojoData, setEmojiData] = useState(() => {
    // if (values.emojiIconId) {
    //     return getEmoji(values.emojiIconId);
    // }
    // return null;
    // });

    // const openIconPicker = () => {
    //     setShownIconPicker(prev => ({ shownIconPicker: !prev.shownIconPicker }));
    // }

    // const updateIcon = (emoji) => {
    //     setEmojiData(emoji);
    //     setFieldValue('emojiIconId', emoji.id);
    //     setShownIconPicker(false);
    // }

    const theme = useTheme();
    const desktopMatches = useMediaQuery(theme.breakpoints.up('sm'));

    const layersVals = Object.values(layers);
    const currLayerVal = (values.layer && values.layer.value) || values.layer || '';

    const formDisabled = isSubmitting || !dirty || isEmpty(values) || every(values, property => isEmpty(property));
    return <form onSubmit={handleSubmit} noValidate autoComplete="off">
        <Grid container>
            <Grid item xs={12} md={6}>
                <FormControl className={classes.formControl} error={!isEmpty(errors.layer)}>
                    <InputLabel htmlFor="layer">Nature</InputLabel>
                    <Select
                        value={currLayerVal}
                        onChange={handleChange}
                        inputProps={{
                            name: 'layer',
                            id: 'layer',
                        }}
                    >
                        {
                            layersVals.map(l => <MenuItem key={l.id} value={l.value}>
                                {/* {l.emojiIconId && <Emoji emoji={getEmoji(l.emojiIconId)} />}{l.label} */}
                                {l.emojiIconId && getEmoji(l.emojiIconId).native} {l.label}
                            </MenuItem>)
                        }
                    </Select>
                    {errors.layer && <FormHelperText>{errors.layer}</FormHelperText>}
                </FormControl>
            </Grid>
            {/* <Grid item xs={12} md={6}>
                 {emojoData && emojoData.emojiIconId && <Emoji {...emojoData} onClick={openIconPicker} />} 
                {/* <TextField
            label="Pick one icon"
            name='emojiIconId'
            value={values.emojiIconId ? emojiIcon.native : ' - '}
            // onChange={handleChange}
            onClick={openIconPicker}
            error={!isEmpty(errors.emojiIconId)}
            helperText={errors.emojiIconId}
            disabled
        />      
         <FormControl className={classes.formControl} error={!isEmpty(errors.emojiIconId)}>
            <InputLabel>Pick one icon</InputLabel>
            <IconButton className={classes.iconButton} onClick={openIconPicker}>
                {values.emojiIconId && emojiIcon.native}
                {!values.emojiIconId && " - "}
            </IconButton>
            <FormHelperText>{errors.emojiIconId}</FormHelperText>
        </FormControl>
             {
                    shownIconPicker &&
                    <div>
                        <Picker onSelect={updateIcon} />
                    </div>
                } 
        </Grid> */}
            <Grid item xs={12}>
                <TextField
                    label="Marker Title"
                    name='title'
                    value={values.title || ''}
                    onChange={handleChange}
                    error={!isEmpty(errors.title)}
                    helperText={errors.title}
                />
            </Grid>
            <Grid item xs={12}>
                <TextField
                    label="Description"
                    name='description'
                    value={values.description || ''}
                    onChange={handleChange}
                    error={!isEmpty(errors.description)}
                    helperText={errors.description}
                />
            </Grid>
            <Grid item xs={12}>
                {
                    values.updatedAt &&
                    <Typography>
                        Updated at {' '}
                        <TimeAgo
                            datetime={new Date(values.updatedAt * 1000)}
                            locale='en-US' />
                    </Typography>
                }
            </Grid>
        </Grid>
        <Grid item xs={12}>
            <Grid container direction="row" justify="flex-end" alignItems="center" className={classes.marginTopItem}>
                <Button variant="contained" color="secondary" className={classes.button} onClick={handleDelete}>
                    Delete
                                <DeleteIcon className={classes.rightIcon} />
                </Button>
                <Button variant="contained" color="secondary" className={classes.button} onClick={handleCancel}>
                    Cancel
                                <CancelIcon className={classes.rightIcon} />
                </Button>
                <Button type="submit" variant="contained" color="primary" className={classes.button} disabled={formDisabled}>
                    Save
                                <SaveIcon className={classes.rightIcon} />
                </Button>
            </Grid>
        </Grid>
    </form >;
}
InnerForm.propTypes = {
    values: PropTypes.object.isRequired,
}

const Form = withFormik({
    enableReinitialize: true,
    mapPropsToValues: props => props.model,
    validate: values => {
        const errors = {};
        if (!values.layer) {
            errors.layer = "Required";
        }
        if (!values.title) {
            errors.title = "Required";
        }
        return errors;
    },
    handleSubmit: (values, formikBag) => formikBag.props.handleSubmit(values, formikBag)
})(InnerForm);

const ViewCard = ({ values, layers }) => {
    let emojiIcon = null;
    if (values.emojiIconId) {
        emojiIcon = getEmoji(values.emojiIconId);
    }

    const layer = Object.keys(layers).find(l => l === values.layer);
    const layerDesc = layer ? layers[layer] : null;
    return <Grid container spacing={3}>
        <Grid item xs={12} >
            {layerDesc && layerDesc.label}
        </Grid>
        <Grid item xs={12} >
            <Grid>
                {values.emojiIconId && emojiIcon.native}
                {!values.emojiIconId && " - "}
            </Grid>
        </Grid>
        <Grid item xs={12}>
            {values.title}
        </Grid>
        <Grid item xs={12}>
            {values.description}
        </Grid>
        {
            values.updatedAt && <Grid item xs={12}>
                <Typography>
                    <TimeAgo
                        datetime={new Date(values.updatedAt * 1000)}
                        locale='en-US' />
                </Typography>
            </Grid>
        }
    </Grid>;
}
ViewCard.propTypes = {
    values: PropTypes.object.isRequired,
}


const MarkerEditPanel = ({ isSignedIn, user, layers, mapId, marker, saveAction, cancelAction, deleteAction }) => {

    const defaultValue = { map: null, title: '', description: '', layer: '', emojiIconId: '' };

    const markerData = Object.assign({}, defaultValue, marker);

    const classes = useStyles();

    const viewModel = !isSignedIn || !user || !saveAction;

    const theme = useTheme();
    const desktopMatches = useMediaQuery(theme.breakpoints.up('sm'));

    return <Card className={desktopMatches ? classes.controlRight : classes.controlBottom}>
        <CardHeader variant="h4" title={viewModel ? markerData.title : (markerData.id ? "Edit Marker " + markerData.title : "Create Marker")}
            action={
                <IconButton aria-label="Settings" onClick={cancelAction}>
                    <CancelIcon />
                </IconButton>
            } />
        <CardContent className={classes.cardContent}>
            {viewModel && <ViewCard layers={layers} values={markerData} />}
            {!viewModel && markerData && <Form layers={layers} model={markerData}
                handleSubmit={saveAction}
                handleCancel={cancelAction}
                handleDelete={deleteAction} />
            }
        </CardContent>
    </Card>;
}

MarkerEditPanel.propTypes = {
    isSignedIn: PropTypes.bool,
    user: PropTypes.object
}

const mapStateToProps = (state) => ({
    layers: state.screen.map.layers,
});
export default connect(mapStateToProps)(MarkerEditPanel);
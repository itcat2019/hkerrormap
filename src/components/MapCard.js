import React, { useEffect } from 'react';
import { Typography } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { Card, CardMedia, CardContent } from '@material-ui/core';
import FavoriteIcon from '@material-ui/icons/ThumbUp';
import { makeStyles } from '@material-ui/styles';
import { get } from 'lodash';
import API, { graphqlOperation } from '@aws-amplify/api'
import Button from './TooltipIconButton'
import { updateMap } from '../graphql/mutations';

const useStyles = makeStyles(theme => ({
  card: {
    display: 'flex',
    minHeight: 200,
  },
  thumbnail: {
    minWidth: 200,
  },
  content: {
    padding: 25,
    objectFit: 'cover',
  },
}));

const MapCard = ({ map }) => {
  const classes = useStyles();

  useEffect(() => {
    if (!map.url) {
      const val = Object.assign({}, map);
      let update = false;
      if (!val.vote) {
        val.vote = { upvote: 0 };
        update = true;
      }
      if (update) {
        delete val.markers
        API.graphql(graphqlOperation(updateMap, { input: val }));
      }
    }
  });

  const upvote = async () => {
    const val = Object.assign({}, map);
    delete val.markers
    val.vote.upvote += 1;
    const newMapVoteData = await API.graphql(graphqlOperation(updateMap, { input: val }));
  }

  return (
    <Card className={classes.card}>
      <CardMedia
        image={map.thumbUrl}
        title="Map thumbnail"
        className={classes.thumbnail}
      />
      <CardContent className={classes.content}>
        <Link to={map.url ? map.url : `/maps/${map.id}`} disabled={map.disabled}>
          <Typography
            variant="h5"
            color="primary"
          >
            {map.title}
          </Typography>
        </Link>
        <Typography variant="body2" color="textSecondary">
          {map.startAt}
        </Typography>
        <Typography variant="body1">{map.description}</Typography>
        <Button onClick={upvote} tip="Click to +1">
          <FavoriteIcon color="primary" />
        </Button>
        <span>{get(map, 'vote.upvote', 0)} UpVote</span>
      </CardContent>
    </Card>
  );
}

export default MapCard;
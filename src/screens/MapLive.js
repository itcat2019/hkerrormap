import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { useImmer } from "use-immer";
import { API, graphqlOperation } from 'aws-amplify';
import { makeStyles, createStyles } from '@material-ui/styles';
import Box from '@material-ui/core/Box';
import Slider from '@material-ui/core/Slider';
import IconButton from '@material-ui/core/IconButton';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import PauseIcon from '@material-ui/icons/Pause';
import StopIcon from '@material-ui/icons/Stop';
import { Map as LeafletMap, FeatureGroup, LayersControl, LayerGroup } from 'react-leaflet';
import { Icon } from 'leaflet';
import axios from 'axios';
import { format } from 'date-fns';
import PouchDBTileLayer from 'react-leaflet-pouchdb-tilelayer'
import Marker from '../components/Marker';
import MarkerEditPanel from '../components/MarkerEditPanel';
import LaunchScreen from '../layout/LaunchScreen/LaunchScreen';

import { END_CREATE_MARKER, MARKERS_CHANGE, LAYERS_CHANGE, END_EDIT_MARKER, START_CREATE_MARKER } from '../redux/reducers/screenReducer';

import { listLayers } from '../graphql/queries'

import 'leaflet/dist/leaflet.css';
// https://github.com/PaulLeCam/react-leaflet/issues/255
// stupid hack so that leaflet's images work after going through webpack
import marker from 'leaflet/dist/images/marker-icon.png';
import marker2x from 'leaflet/dist/images/marker-icon-2x.png';
import markerShadow from 'leaflet/dist/images/marker-shadow.png';
import layers from 'leaflet/dist/images/layers.png';
import layers2x from 'leaflet/dist/images/layers-2x.png';

delete Icon.Default.prototype._getIconUrl;

Icon.Default.mergeOptions({
  iconRetinaUrl: marker2x,
  iconUrl: marker,
  shadowUrl: markerShadow
});

const useStyles = makeStyles(theme => ({
  mapContainer: {
    height: 'calc(100vh - 64px)',
  },
  map: {
    width: '100%',
    height: '100%',
    zIndex: 0,
  },
  slider: {
    width: 300,
  },
}));

const MapLiveScreen = ({
  match,
  editingMarker,
  pickerPosition,
  setLocationPicker,
  unsetLocationPicker,
  changeLayer,
  changeMarker,
  clearEdit
}) => {
  const classes = useStyles();
  const [ready, setReady] = useState(false);
  const [playing, setPlaying] = useState(false);
  const [markersData, setMarkersData] = useImmer([]);
  const [playingTime, setPlayingTime] = useState(0);
  const [timer, setTimer] = useState(null);
  const [startDt, setStartDt] = useState(Math.floor(new Date() / 1000));
  const [currentStartDt, setCurrrentStartDt] = useState(Math.floor(new Date() / 1000));
  const mapId = match.params.mapId;

  const getData = async () => {
    const layersData = await API.graphql(graphqlOperation(listLayers))
    changeLayer("created", layersData.data.listLayers.items);

    const s = Math.floor(new Date() / 1000 - 60 * 30);
    const e = Math.floor(new Date() / 1000);
    const response = await axios.get(`http://a94d2eb2cb27b11e9834b0621e18a9c1-1701325265.ap-southeast-1.elb.amazonaws.com/?start_dt=${s}&end_dt=${e}`);
    const data = response.data.sort(compareMarker);
    setMarkersData(draft => {
      data.forEach(d => draft.push(d));
    });
    setReady(true);
  }

  useEffect(() => {
    getData();
  }, []);

  useEffect(() => {
    let timer = null;
    (async () => {
      const endDt = Math.floor(currentStartDt + 10);
      const response = await axios.get(`http://a94d2eb2cb27b11e9834b0621e18a9c1-1701325265.ap-southeast-1.elb.amazonaws.com/?start_dt=${currentStartDt}&end_dt=${endDt}`);
      const data = response.data.sort(compareMarker);
      setMarkersData(draft => {
        data.forEach(d => {
          if (data.findIndex(dd => dd.id === d.id) < 0) {
            draft.push(d);
          }
        });
      });
      setReady(true);
      timer = setTimeout(() => {
        setCurrrentStartDt(Math.floor(new Date() / 1000));
      }, 3000);
    })();
    return () => {
      timer && clearTimeout(timer);
    };
  }, [currentStartDt]);

  function compareMarker(a, b) {
    return a.declaredMessageDt - b.declaredMessageDt;
  }


  const handlePlay = (event, newValue) => {
    play();
  };
  const handlePause = (event, newValue) => {
    stop();
  };
  const handleTimeSliderChange = (event, newValue) => {
    const playingStatus = playing;
    stop();
    const endDt = Math.floor(new Date() / 1000);
    const newPlayingTime = Math.floor((endDt - startDt) / 100 * newValue);
    setPlayingTime(newPlayingTime);
    if (playingStatus) {
      setTimeout(() => play(), 1000);
    }
  };

  const speed = 3;
  const play = () => {
    const t = setInterval(() => {
      const endDt = Math.floor(new Date() / 1000);
      if (playingTime > (endDt - startDt)) {
        stop();
        return;
      }
      setPlayingTime(t => {
        return t + 1;
      });
    }, 1000 / speed);
    setTimer(t);
    setPlaying(true);
  }
  const stop = () => {
    setPlaying(false);
    timer && clearInterval(timer);
    setTimer(null);
  }

  const convertMessageToMarker = (m) => {
    let emojiIconId = 'question';
    if (m.luisResult) {
      if (m.luisResult['dog-vehicle']) {
        emojiIconId = 'police_car';
      } else if (m.luisResult['safe']) {
        emojiIconId = 'heavy_check_mark';
      }
    }
    const position = m.geocode ? {
      latitude: m.geocode.lat,
      longitude: m.geocode.lng
    } : null;
    return {
      id: m.id,
      title: m.messageText,
      layer: { emojiIconId: emojiIconId },
      updateAt: m.declaredMessageDt,
      position: position
    }
  }

  const keepOnScreenSec = 60 * 30;
  const selectedMarkers = markersData.filter(d => d.declaredMessageDt > (startDt + playingTime - keepOnScreenSec) && d.declaredMessageDt <= (startDt + playingTime)).map(m => convertMessageToMarker(m));
  const selectedMarker = editingMarker ? convertMessageToMarker(markersData.find(m => m.id === editingMarker)) : null;
  const endDt = Math.floor(new Date() / 1000);
  const timeSliderValue = playingTime / (endDt - startDt) * 100;
  // this will render Hong Kong on map
  const defaultCenter = [22.3528, 114.1600];
  const defaultZoom = 13;
  if (!ready) {
    return <LaunchScreen />;
  }
  return (
    <div>
      <div>
        <div className={classes.mapContainer}>
          <LeafletMap
            zoom={defaultZoom}
            center={defaultCenter}
            className={classes.map}
          >
            <LayersControl position="topleft">
              <LayersControl.BaseLayer checked name="OSM Color">
                <PouchDBTileLayer
                  useCache={true}
                  crossOrigin={true}
                  attribution='&copy; <a href="http://openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                  url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
              </LayersControl.BaseLayer>
              <LayersControl.BaseLayer name="OSM Grey Scale">
                <PouchDBTileLayer
                  useCache={true}
                  crossOrigin={true}
                  attribution='&copy; <a href="http://openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                  url="https://tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png"
                />
              </LayersControl.BaseLayer>
              <LayersControl.Overlay checked name='Marker'>
                <FeatureGroup>
                  {selectedMarkers.map(marker => <Marker key={marker.id} marker={marker} layer={marker.layer} />)}
                </FeatureGroup>
              </LayersControl.Overlay>
            </LayersControl>
          </LeafletMap>
        </div>
        {editingMarker && <MarkerEditPanel
          marker={selectedMarker}
          cancelAction={clearEdit}
        />}

      </div>

    </div >
  );
}

const mapStateToProps = (state) => ({
  editingMarker: state.screen.map.editingMarker,
  pickerPosition: state.screen.map.pickerPosition,

  layers: state.screen.map.layers,
});
const mapDispatchToProps = (dispatch) => ({
  setLocationPicker: (latlng) => dispatch({ type: START_CREATE_MARKER, pickerPosition: latlng }),
  unsetLocationPicker: () => dispatch({ type: END_CREATE_MARKER }),
  clearEdit: () => dispatch({ type: END_EDIT_MARKER }),
  changeMarker: (changeType, markers) => dispatch({ type: MARKERS_CHANGE, changeType, markers }),
  changeLayer: (changeType, layers) => dispatch({ type: LAYERS_CHANGE, changeType, layers }),
});
export default connect(mapStateToProps, mapDispatchToProps)(MapLiveScreen);
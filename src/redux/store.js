import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import screenReducer from './reducers/screenReducer';

const initialState = { };

const middleware = [ ];

const reducers = combineReducers({
  screen: screenReducer,
});

const composeEnhancers =
  typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
    : compose;

const enhancer = composeEnhancers(applyMiddleware(...middleware));
const store = createStore(reducers, initialState, enhancer);

export default store;
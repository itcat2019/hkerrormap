// eslint-disable
// this is an auto generated file. This will be overwritten

export const createMap = `mutation CreateMap($input: CreateMapInput!) {
  createMap(input: $input) {
    id
    defaultCenter {
      latitude
      longitude
    }
    defaultZoom
    description
    thumbUrl
    title
    vote {
      upvote
    }
    markers {
      items {
        id
        emojiIconId
        description
        title
        createdAt
        updatedAt
        createUserID
        createUserEmail
        updateUserID
        updateUserEmail
      }
      nextToken
    }
  }
}
`;
export const updateMap = `mutation UpdateMap($input: UpdateMapInput!) {
  updateMap(input: $input) {
    id
    defaultCenter {
      latitude
      longitude
    }
    defaultZoom
    description
    thumbUrl
    title
    vote {
      upvote
    }
    markers {
      items {
        id
        emojiIconId
        description
        title
        createdAt
        updatedAt
        createUserID
        createUserEmail
        updateUserID
        updateUserEmail
      }
      nextToken
    }
  }
}
`;
export const deleteMap = `mutation DeleteMap($input: DeleteMapInput!) {
  deleteMap(input: $input) {
    id
    defaultCenter {
      latitude
      longitude
    }
    defaultZoom
    description
    thumbUrl
    title
    vote {
      upvote
    }
    markers {
      items {
        id
        emojiIconId
        description
        title
        createdAt
        updatedAt
        createUserID
        createUserEmail
        updateUserID
        updateUserEmail
      }
      nextToken
    }
  }
}
`;
export const createLayer = `mutation CreateLayer($input: CreateLayerInput!) {
  createLayer(input: $input) {
    id
    label
    value
    emojiIconId
  }
}
`;
export const updateLayer = `mutation UpdateLayer($input: UpdateLayerInput!) {
  updateLayer(input: $input) {
    id
    label
    value
    emojiIconId
  }
}
`;
export const deleteLayer = `mutation DeleteLayer($input: DeleteLayerInput!) {
  deleteLayer(input: $input) {
    id
    label
    value
    emojiIconId
  }
}
`;
export const createMarker = `mutation CreateMarker($input: CreateMarkerInput!) {
  createMarker(input: $input) {
    id
    map {
      id
      defaultCenter {
        latitude
        longitude
      }
      defaultZoom
      description
      thumbUrl
      title
      vote {
        upvote
      }
      markers {
        nextToken
      }
    }
    layer {
      id
      label
      value
      emojiIconId
    }
    position {
      latitude
      longitude
    }
    emojiIconId
    description
    title
    createdAt
    updatedAt
    createUserID
    createUserEmail
    updateUserID
    updateUserEmail
  }
}
`;
export const updateMarker = `mutation UpdateMarker($input: UpdateMarkerInput!) {
  updateMarker(input: $input) {
    id
    map {
      id
      defaultCenter {
        latitude
        longitude
      }
      defaultZoom
      description
      thumbUrl
      title
      vote {
        upvote
      }
      markers {
        nextToken
      }
    }
    layer {
      id
      label
      value
      emojiIconId
    }
    position {
      latitude
      longitude
    }
    emojiIconId
    description
    title
    createdAt
    updatedAt
    createUserID
    createUserEmail
    updateUserID
    updateUserEmail
  }
}
`;
export const deleteMarker = `mutation DeleteMarker($input: DeleteMarkerInput!) {
  deleteMarker(input: $input) {
    id
    map {
      id
      defaultCenter {
        latitude
        longitude
      }
      defaultZoom
      description
      thumbUrl
      title
      vote {
        upvote
      }
      markers {
        nextToken
      }
    }
    layer {
      id
      label
      value
      emojiIconId
    }
    position {
      latitude
      longitude
    }
    emojiIconId
    description
    title
    createdAt
    updatedAt
    createUserID
    createUserEmail
    updateUserID
    updateUserEmail
  }
}
`;

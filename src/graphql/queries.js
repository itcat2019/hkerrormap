// eslint-disable
// this is an auto generated file. This will be overwritten

export const getMap = `query GetMap($id: ID!) {
  getMap(id: $id) {
    id
    defaultCenter {
      latitude
      longitude
    }
    defaultZoom
    description
    thumbUrl
    title
    vote {
      upvote
    }
    markers {
      items {
        id
        emojiIconId
        description
        title
        createdAt
        updatedAt
        createUserID
        createUserEmail
        updateUserID
        updateUserEmail
      }
      nextToken
    }
  }
}
`;
export const listMaps = `query ListMaps($filter: ModelMapFilterInput, $limit: Int, $nextToken: String) {
  listMaps(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      defaultCenter {
        latitude
        longitude
      }
      defaultZoom
      description
      thumbUrl
      title
      vote {
        upvote
      }
      markers {
        nextToken
      }
    }
    nextToken
  }
}
`;
export const getLayer = `query GetLayer($id: ID!) {
  getLayer(id: $id) {
    id
    label
    value
    emojiIconId
  }
}
`;
export const listLayers = `query ListLayers(
  $filter: ModelLayerFilterInput
  $limit: Int
  $nextToken: String
) {
  listLayers(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      label
      value
      emojiIconId
    }
    nextToken
  }
}
`;
export const getMarker = `query GetMarker($id: ID!) {
  getMarker(id: $id) {
    id
    map {
      id
      defaultCenter {
        latitude
        longitude
      }
      defaultZoom
      description
      thumbUrl
      title
      vote {
        upvote
      }
      markers {
        nextToken
      }
    }
    layer {
      id
      label
      value
      emojiIconId
    }
    position {
      latitude
      longitude
    }
    emojiIconId
    description
    title
    createdAt
    updatedAt
    createUserID
    createUserEmail
    updateUserID
    updateUserEmail
  }
}
`;
export const listMarkers = `query ListMarkers(
  $filter: ModelMarkerFilterInput
  $limit: Int
  $nextToken: String
) {
  listMarkers(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      map {
        id
        defaultZoom
        description
        thumbUrl
        title
      }
      layer {
        id
        label
        value
        emojiIconId
      }
      position {
        latitude
        longitude
      }
      emojiIconId
      description
      title
      createdAt
      updatedAt
      createUserID
      createUserEmail
      updateUserID
      updateUserEmail
    }
    nextToken
  }
}
`;

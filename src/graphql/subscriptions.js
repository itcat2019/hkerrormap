// eslint-disable
// this is an auto generated file. This will be overwritten

export const onCreateMap = `subscription OnCreateMap {
  onCreateMap {
    id
    defaultCenter {
      latitude
      longitude
    }
    defaultZoom
    description
    thumbUrl
    title
    vote {
      upvote
    }
    markers {
      items {
        id
        emojiIconId
        description
        title
        createdAt
        updatedAt
        createUserID
        createUserEmail
        updateUserID
        updateUserEmail
      }
      nextToken
    }
  }
}
`;
export const onUpdateMap = `subscription OnUpdateMap {
  onUpdateMap {
    id
    defaultCenter {
      latitude
      longitude
    }
    defaultZoom
    description
    thumbUrl
    title
    vote {
      upvote
    }
    markers {
      items {
        id
        emojiIconId
        description
        title
        createdAt
        updatedAt
        createUserID
        createUserEmail
        updateUserID
        updateUserEmail
      }
      nextToken
    }
  }
}
`;
export const onDeleteMap = `subscription OnDeleteMap {
  onDeleteMap {
    id
    defaultCenter {
      latitude
      longitude
    }
    defaultZoom
    description
    thumbUrl
    title
    vote {
      upvote
    }
    markers {
      items {
        id
        emojiIconId
        description
        title
        createdAt
        updatedAt
        createUserID
        createUserEmail
        updateUserID
        updateUserEmail
      }
      nextToken
    }
  }
}
`;
export const onCreateLayer = `subscription OnCreateLayer {
  onCreateLayer {
    id
    label
    value
    emojiIconId
  }
}
`;
export const onUpdateLayer = `subscription OnUpdateLayer {
  onUpdateLayer {
    id
    label
    value
    emojiIconId
  }
}
`;
export const onDeleteLayer = `subscription OnDeleteLayer {
  onDeleteLayer {
    id
    label
    value
    emojiIconId
  }
}
`;
export const onCreateMarker = `subscription OnCreateMarker {
  onCreateMarker {
    id
    map {
      id
      defaultCenter {
        latitude
        longitude
      }
      defaultZoom
      description
      thumbUrl
      title
      vote {
        upvote
      }
      markers {
        nextToken
      }
    }
    layer {
      id
      label
      value
      emojiIconId
    }
    position {
      latitude
      longitude
    }
    emojiIconId
    description
    title
    createdAt
    updatedAt
    createUserID
    createUserEmail
    updateUserID
    updateUserEmail
  }
}
`;
export const onUpdateMarker = `subscription OnUpdateMarker {
  onUpdateMarker {
    id
    map {
      id
      defaultCenter {
        latitude
        longitude
      }
      defaultZoom
      description
      thumbUrl
      title
      vote {
        upvote
      }
      markers {
        nextToken
      }
    }
    layer {
      id
      label
      value
      emojiIconId
    }
    position {
      latitude
      longitude
    }
    emojiIconId
    description
    title
    createdAt
    updatedAt
    createUserID
    createUserEmail
    updateUserID
    updateUserEmail
  }
}
`;
export const onDeleteMarker = `subscription OnDeleteMarker {
  onDeleteMarker {
    id
    map {
      id
      defaultCenter {
        latitude
        longitude
      }
      defaultZoom
      description
      thumbUrl
      title
      vote {
        upvote
      }
      markers {
        nextToken
      }
    }
    layer {
      id
      label
      value
      emojiIconId
    }
    position {
      latitude
      longitude
    }
    emojiIconId
    description
    title
    createdAt
    updatedAt
    createUserID
    createUserEmail
    updateUserID
    updateUserEmail
  }
}
`;

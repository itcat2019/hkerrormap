const workboxBuild = require("workbox-build");

// NOTE: This should be run *AFTER* all your assets are built
const buildSW = () => {
  console.log(`workbox-build with ${process.env.NODE_ENV}`)
  const swOptions = {
    globDirectory: "build",
    globIgnores: ["**/.map", "**/asset-manifest.json"],
    swDest: process.env.NODE_ENV === "production" ? "build/sw-custom.js" : "public/sw-custom-dev.js",
    skipWaiting: true,
    clientsClaim: true,
    importWorkboxFrom: "cdn",
    navigateFallbackBlacklist: [
      // Exclude URLs starting with /_, as they're likely an API call
      new RegExp('^/_'),
      // Exclude URLs containing a dot, as they're likely a resource in
      // public/ and not a SPA route
      new RegExp('/[^/]+\\.[^/]+$'),
    ],
    importScripts: [],
    runtimeCaching: [
      {
        // To match cross-origin requests, use a RegExp that matches
        // the start of the origin:
        urlPattern: new RegExp('^https://[a-z]\.tile\.openstreetmap\.org/'),
        handler: 'CacheFirst',
        options: {
          cacheableResponse: {
            statuses: [0, 200]
          }
        }
      },
      {
        urlPattern: /\.(?:png|gif|jpg|jpeg|webp|svg|woff|woff2)$/,
        handler: 'CacheFirst',
        options: {
          cacheableResponse: {
            statuses: [0, 200]
          }
        }
      }]
  };

  if (process.env.NODE_ENV === "production") {
    swOptions.navigateFallback = "/index.html";
  }

  // https://github.com/facebook/create-react-app/blob/8e32ce57d01e2e8e68a4ee7da1752509d9733991/packages/react-scripts/config/webpack.config.js#L618
  // This will return a Promise
  return workboxBuild.generateSW(swOptions);
};
buildSW();
